window.addEventListener("load", init);

//Variables
let lyrics;
let artist;
let track;

let profanityURL = 'https://www.purgomalum.com/service/json';
let profanityList = 'sex, sexy, nigga, nigger, niggers, niggas, cock, cocks, dick, hoe';
let fillText = 'bobba';

//Html
let artistField = document.getElementById("artist");
let trackField = document.getElementById("track");
let artistInfo = document.getElementById("artist-info");
let trackInfo = document.getElementById("track-info");
let resultLyrics = document.getElementById('result-lyrics');
let p = document.getElementById("lyrics");
let error = document.getElementById("error");
let loading = document.getElementById("loading");
let progressBar = document.getElementById("progress-bar");
let profanitySection = document.getElementById("profanity");

function init() {
    resultLyrics.style.display = 'none';
    error.style.display = 'none';
    loading.style.display = 'none';
    profanitySection.style.display = 'none';

    document.getElementById("button").addEventListener("click", validateForm);

    document.getElementById('track').onkeydown = function (event) {
        if (event.keyCode == 13) {
            validateForm();
        }
    };
    document.getElementById('artist').onkeydown = function (event) {
        if (event.keyCode == 13) {
            validateForm();
        }
    };
}

function validateForm() {

    artist = artistField.value;
    track = trackField.value;

    //Log the current Artist and Track
    console.log(artist, track);

    if (artist === "" && !($('#artist').hasClass('is-invalid'))) {
        artistField.className += " is-invalid";
    }

    if (artist && $('#artist').hasClass('is-invalid')) {
        artistField.classList.remove("is-invalid");
    }

    if (track === "" && !($('#track').hasClass('is-invalid'))) {
        trackField.className += " is-invalid";
    }

    if (track && $('#track').hasClass('is-invalid')) {
        trackField.classList.remove("is-invalid");
    }

    //Check if both fields has value
    if (artist && track) {
        //Remove .is-invalid class if it's added to input fields
        if ($('#artist').hasClass('is-invalid') || $('#track').hasClass('is-invalid')) {
            artistField.classList.remove("is-invalid");
            trackField.classList.remove("is-invalid");
        }
        //Finding lyrics...
        sendDataToLyricsAPI();
    }

}

const sendDataToLyricsAPI = () => {

    //Lyrics API Request
    $.getJSON('https://api.lyrics.ovh/v1/' + artist + '/' + track)
        .done(getLyrics)
        .fail(function (e) {
            resultLyrics.style.display = 'none';
            profanitySection.style.display = 'none';
            error.innerHTML = "Error! Can't find the song!";
            error.style.display = 'block';
        });
};

const getLyrics = (data) => {

    //Hide error messages
    error.style.display = 'none';

    //Set the lyrics from the api into the global lyrics
    lyrics = data.lyrics;
    //adds the breaking points in the lyrics
    lyrics = lyrics.replace(/(?:\r\n|\r|\n)/g, '<br>');

    //Fill in the artis and track name
    artistField.innerHTML = artist;
    trackField.innerHTML = track;

    //Censor the lyrics
    checkLyricsforProfanity();
};

const checkLyricsforProfanity = () => {

    $.ajax({
        url: profanityURL + '?text=' + lyrics + '&add=' + profanityList + '&fill_text=' + fillText,
        beforeSend: function () {
            $('#loading').show();
            loading.style.display = 'display';
        },
        complete: function () {
            loading.style.display = 'none';
        },
        success: successCallbackHandler,
        error: function () {
            alert("Error: Profanity API Request failed, please try later again.");
        }
    });

};

const successCallbackHandler = (lyricsCensored) => {

    let result = lyricsCensored.result;
    //make bobba red
    result = result.replace(new RegExp(fillText, 'g'), '<span style="color: #eb6651;">' + fillText + '</span>');
    const profanityCount = WordCount(result);
    let artistLength = artist.length;

    //Singer and track putting underside of the lyrics
    artistInfo.innerHTML = artist;
    trackInfo.innerHTML = track;

    let progressCount = 100;
    let progressColor = 'bg-success';

    if (profanityCount >= 20) {
        progressColor = 'bg-danger';
    } else if (profanityCount >= 10) {
        //rood
        progressCount = profanityCount * 5;
        progressColor = 'bg-danger';
    } else if (profanityCount >= 1) {
        //geel
        progressCount = profanityCount * 5;
        progressColor = 'bg-warning';
    }

    progressBar.innerHTML = `
        <p>This song contains ${profanityCount} bad words.</p>
        <div class="row"><div class="col-6">0</div><div class="col-6"><div class="float-right">20+</div></div></div>
        <div class="progress">
            <div class="progress-bar ${progressColor}" role="progressbar" style="width: ${progressCount}%" aria-valuenow="${progressCount}" aria-valuemin="0" aria-valuemax="20"></div>
        </div>`;

    //Check if the lyrics contains a french sentence
    if (result.includes("Paroles")) {

        // var n = str.indexOf(artist);
        // var res = str.substring(0, n + artistLength);
        // var repl = profanity.result.replace(res, '');
        //
        // p.innerHTML = repl;
        // resultLyrics.style.display = 'block';

        p.innerHTML = result;
        resultLyrics.style.display = 'block';
    }

    p.innerHTML = result;
    profanitySection.style.display = 'block';
    resultLyrics.style.display = 'block';

};

const WordCount = (str) => {
    let count = str.split("bobba").length;
    return count - 1;
};